%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global auditversion 0.3

Summary: A set of tools to gather troubleshooting information from a system
Name: sos
Version: 4.1
Release: 5%{?dist}
Group: Applications/System
Source0: https://github.com/sosreport/sos/archive/%{version}/sos-%{version}.tar.gz
Source1: sos-audit-%{auditversion}.tgz
License: GPLv2+
BuildArch: noarch
Url: https://github.com/sosreport/sos
BuildRequires: python3-devel
BuildRequires: gettext
Requires: libxml2-python3
#Requires: python3-rpm
Requires: tar
Requires: bzip2
Requires: xz
Recommends: python3-pexpect
Conflicts: vdsm < 4.40
Obsoletes: sos-collector <= 1.9
Patch1: sos-bz1925419-gluster-pubkeys-statusfile.patch
Patch2: sos-bz1967110-collect-cleaning-consistency.patch
Patch3: sos-bz1967111-manpages-see-also.patch
Patch4: sos-bz1967112-add-cmd-timeout.patch
Patch5: sos-bz1967113-ds-mask-password-in-ldif.patch
Patch6: sos-bz1967114-gather-cups-browsed-logs.patch
Patch7: sos-bz1967115-sssd-memcache-and-logs.patch
Patch8: sos-bz1967116-ibmvNIC-dynamic-debugs.patch
Patch9: sos-bz1967117-pulpcore-plugin.patch
Patch10: sos-bz1967118-saphana-traceback.patch
Patch11: sos-bz1967119-collect-nstat.patch
Patch12: sos-bz1967120-snapper-plugin-and-allocation-failures.patch
Patch13: sos-bz1965002-skip-selinux-of-proc-everytime.patch

%description
Sos is a set of tools that gathers information about system
hardware and configuration. The information can then be used for
diagnostic purposes and debugging. Sos is commonly used to help
support technicians and developers.

%prep
%setup -qn %{name}-%{version}
%setup -T -D -a1 -q
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
%patch5 -p1
%patch6 -p1
%patch7 -p1
%patch8 -p1
%patch9 -p1
%patch10 -p1
%patch11 -p1
%patch12 -p1
%patch13 -p1

%build
%py3_build

%install
%py3_install '--install-scripts=%{_sbindir}'

install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}
install -d -m 700 %{buildroot}%{_sysconfdir}/%{name}/cleaner
install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}/presets.d
install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}/groups.d
install -d -m 755 %{buildroot}%{_sysconfdir}/%{name}/extras.d
install -m 644 %{name}.conf %{buildroot}%{_sysconfdir}/%{name}/%{name}.conf

rm -rf %{buildroot}/usr/config/

%find_lang %{name} || echo 0

cd %{name}-audit-%{auditversion}
DESTDIR=%{buildroot} ./install.sh
cd ..

%files -f %{name}.lang
%{_sbindir}/sos
%{_sbindir}/sosreport
%{_sbindir}/sos-collector
#%dir /etc/sos/cleaner
%dir /etc/sos/presets.d
%dir /etc/sos/extras.d
%dir /etc/sos/groups.d
%{python3_sitelib}/*
%{_mandir}/man1/*
%{_mandir}/man5/sos.conf.5.gz
%doc AUTHORS README.md
%license LICENSE
%config(noreplace) %{_sysconfdir}/sos/sos.conf
%config(noreplace) %{_sysconfdir}/sos/cleaner


%package audit
Summary: Audit use of some commands for support purposes
License: GPLv2+
Group: Application/System

%description audit

Sos-audit provides configuration files for the Linux Auditing System
to track the use of some commands capable of changing the configuration
of the system.  Currently storage and filesystem commands are audited.

%post audit
%{_sbindir}/sos-audit.sh

%files audit
%defattr(755,root,root,-)
%{_sbindir}/sos-audit.sh
%defattr(644,root,root,-)
%config(noreplace) %{_sysconfdir}/sos/sos-audit.conf
%defattr(444,root,root,-)
%{_prefix}/lib/sos/audit/*
%{_mandir}/man5/sos-audit.conf.5.gz
%{_mandir}/man8/sos-audit.sh.8.gz
%ghost /etc/audit/rules.d/40-sos-filesystem.rules
%ghost /etc/audit/rules.d/40-sos-storage.rules


%changelog
* Wed Jun 02 2021 Pavel Moravec <pmoravec@redhat.com> - 4.1-4
- [archive] skip copying SELinux context for /proc and /sys everytime
  Resolves: bz1965002
- Load maps from all archives before obfuscation
  Resolves: bz1967110
- Multiple fixes in man pages
  Resolves: bz1967111
- [ds] Mask password and encryption keys in ldif files
  Resolves: bz1967112
- [report] add --cmd-timeout option
  Resolves: bz1967113
- [cups] Add gathering cups-browsed logs
  Resolves: bz1967114
- [sssd] Collect memory cache / individual logfiles
  Resolves: bz1967115
- Collect ibmvNIC dynamic_debugs
  Resolves: bz1967116
- [pulpcore] add plugin for pulp-3
  Resolves: bz1967117
- [saphana] remove redundant unused argument of get_inst_info
  Resolves: bz1967118
- [networking] Add nstat command support
  Resolves: bz1967119
- [snapper] add a new plugin
  Resolves: bz1967120

* Fri Apr 16 2021 Mohan Boddu <mboddu@redhat.com> - 4.1-4
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Thu Apr 01 2021 Pavel Moravec <pmoravec@redhat.com> - 4.1-3
- adding sos-audit
- [gluster] Add glusterd public keys and status files
  Resolves: bz1925419 

* Wed Mar 10 2021 Sandro Bonazzola <sbonazzo@redhat.com> - 4.1-1
- Rebase to 4.1

